import { Injectable } from '@angular/core';
// import { Http } from '@angular/http';
import 'rxjs/add/operator/map';
import { GlobalVariable } from '../app/globals';
import { AlertController, LoadingController } from 'ionic-angular';
import { NativeStorage } from '@ionic-native/native-storage';
import { FirebaseObjectObservable, FirebaseListObservable, AngularFireDatabase } from "angularfire2/database";
import firebase from 'firebase';


@Injectable()
export class FirebaseService {

  userObject: FirebaseObjectObservable<any>;
  userList: FirebaseListObservable<any>;

  constructor(private globals: GlobalVariable,
    private alertCtrl: AlertController,
    private ladingCtrl: LoadingController,
    private ns: NativeStorage,
    public af: AngularFireDatabase) {

    console.log('Hello FirebaseService Provider');

    // this.userObject = af.object('/users/' + this.globals.current_userUID + '/videos/');
    // this.userList = af.list('/users/' + this.globals.current_userUID);
  }
  getUserVideos() {
    let userObject = this.af.object('/users/' + this.globals.current_userUID)
      .subscribe(
      (snapshot) => {
        if (snapshot.videos) {
          this.globals.videos = snapshot.videos
          console.log(this.globals.videos);
        } else this.globals.videos = [];
        userObject.unsubscribe();
      }
      )
  }

  getUserVideosToDisplay() {
    return this.af.list('/users/' + this.globals.current_userUID + '/videos/');
    // return this.af.object('/users/' + this.globals.current_userUID + '/videos/');
  }
  addSingleVideo(key, value): firebase.Promise<any> {
    return this.af.list('/users/' + this.globals.current_userUID + '/videos/').push(value);
    // let userObject = this.af.object('/users/' + this.globals.current_userUID);
    // return userObject.update({ [key]: value });
  }

  getOtherUsersVideos(uid) {
    return this.af.object('/users/' + uid);
  }

  delteVideo(key) {
    return this.af.object('/users/' + this.globals.current_userUID + '/videos/' + key).remove();
  }

  updateMembershipStatus(status: string): firebase.Promise<any> {
    return this.af.object('/users/' + this.globals.current_userUID)
      .update({
        membership: status
      })
  }
  getPaymentStatus() {
    return this.af.object('/users/' + this.globals.current_userUID + '/membership/');
  }
  getPaymentStatusList() {
    return firebase.database().ref('/users/' + this.globals.current_userUID + '/membership/');
    // return this.af.list('/users/' + this.globals.current_userUID + '/membership/')
  }

  getUserData() {
    return this.af.object('/users/' + this.globals.current_userUID);
  }
  /*uploadToFirebase(blobFile) {
    let loader = this.ladingCtrl.create({
      content: "Uploading Video",
    });
    loader.present();

    let alert = this.alertCtrl.create({
      title: "Upload finished",
      buttons: ["OK"]
    });

    let storageRef = firebase.storage().ref();

    let userRef = storageRef.child(this.globals.current_userUID);

    let uploadTask = userRef.put(blobFile)

    var uploadReturn = uploadTask.on('state_changed', (snapshot) => {
      // Observe state change events such as progress, pause, and resume
      // Get task progress, including the number of bytes uploaded and the total number of bytes to be uploaded
      var progress = (snapshot.bytesTransferred / snapshot.totalBytes) * 100;
      console.log('Upload is ' + progress + '% done');

      if (progress == 100) loader.dismiss();
      loader.onDidDismiss(() => alert.present());

      switch (snapshot.state) {
        case firebase.storage.TaskState.PAUSED: // or 'paused'
          console.log('Upload is paused');
          break;
        case firebase.storage.TaskState.RUNNING: // or 'running'
          console.log('Upload is running');
          break;
        case firebase.storage.TaskState.SUCCESS:
          return 'Upload Success 1';
      }
    }, (error) => {
      console.error("error uploading", error);
      return error;
    }, () => {
      var downloadURL = uploadTask.snapshot.downloadURL;
      console.log("downloadURL", downloadURL);
      this.globals.videoURL = downloadURL;
      this.ns.setItem(this.globals.current_userUID, { videoURL: downloadURL });
      return 'Upload Success 2';
    });
    
    return uploadReturn;
  }*/
}
