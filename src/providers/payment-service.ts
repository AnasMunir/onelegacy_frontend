import { Injectable } from '@angular/core';
import { Http, Response, Headers, RequestOptions, URLSearchParams } from '@angular/http';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/toPromise';

/*
  Generated class for the PaymentService provider.

  See https://angular.io/docs/ts/latest/guide/dependency-injection.html
  for more info on providers and Angular 2 DI.
*/
@Injectable()
export class PaymentService {

  constructor(public http: Http) {
    console.log('Hello PaymentService Provider');
  }

  stripePayment(amount, cardToken, description: string): Promise<any> {
    console.log("within stripe payment");
    
    let url = "https://delivery-app-admin.herokuapp.com/api/payment";

    let body = new URLSearchParams();
    body.set("amount", amount);
    body.set("cardToken", cardToken);
    body.set("description", description);

    let headers = new Headers({ 'Content-Type': 'application/x-www-form-urlencoded' });
    let options = new RequestOptions({ headers: headers });
    console.log(body);

    return this.http.post(url, body, options)
      .toPromise()
      .then(this.extractData)
      .catch(this.catchError);
  }

  private extractData(res: Response) {
    return res.json();
  }

  private catchError(error: Response | any): Promise<any> {
    // console.log(error);
    // return Observable.throw(error.json().error || error || "some error in http get");
    return Promise.reject(error.json().error || error || "some error in http post");
  }

}
