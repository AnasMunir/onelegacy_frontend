import { Injectable } from '@angular/core';
import { AngularFireAuth } from 'angularfire2/auth';
import { AngularFireDatabase, FirebaseObjectObservable } from 'angularfire2/database';
import { GlobalVariable } from '../app/globals';
import firebase from 'firebase';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/take';

@Injectable()
export class AuthService {

  public fireAuth: any;
  public userData: any;
  item: FirebaseObjectObservable<any>;
  constructor(
    private afAuth: AngularFireAuth,
    private globals: GlobalVariable,
    private db: AngularFireDatabase) {

    console.log('Hello AuthService Provider');
    afAuth.authState.subscribe(user => {
      if (user) {
        this.fireAuth = user
        console.log(user);
      }
    });
    // this.fireAuth = firebase.auth();

    this.userData = firebase.database().ref('/users');
  }

  loginUser(email: string, password: string): firebase.Promise<any> {
    // return firebase.auth().signInWithEmailAndPassword(email, password);
    return this.afAuth.auth.signInWithEmailAndPassword(email, password);
  }

  async signupUser(email: string, password: string, signupData: any): firebase.Promise<any> {
    const newUser = await this.afAuth.auth.createUserWithEmailAndPassword(email, password)
    this.globals.current_userUID = newUser.uid;
    this.globals.userName = signupData.userName
    await this.userData.child(newUser.uid)
      .set(
      {
        email: email,
        user_name: signupData.userName,
        date_of_birth: signupData.dob,
        contact: signupData.contact,
        address: signupData.address,
        secondaryContact: signupData.secondaryContact
      }
      );
  }

  resetPassword(email: string): any {
    return this.fireAuth.sendPasswordResetEmail(email);
  }

  logoutUser(): firebase.Promise<any> {
    return this.afAuth.auth.signOut();
  }

  // setGlobals(uid) {
  //   this.db.object('/users/' + uid).take(1).subscribe(
  //     (snapshot) => {
  //       console.log('this.globals.firstName', this.globals.firstName);
  //     }
  //   );
  // }
}