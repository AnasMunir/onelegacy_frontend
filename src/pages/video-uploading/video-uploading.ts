import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, Platform, LoadingController, AlertController } from 'ionic-angular';
import { File } from '@ionic-native/file';
import { FirebaseService } from '../../providers/firebase-service';
import { GlobalVariable } from '../../app/globals';
import firebase from 'firebase';

@IonicPage()
@Component({
  selector: 'page-video-uploading',
  templateUrl: 'video-uploading.html',
})
export class VideoUploading {

  title: string;
  description: string;
  fileName: string;
  filePath: string;
  // videos: Array<{ video: string, title: string, description: string }> = [];

  constructor(public navCtrl: NavController,
    public navParams: NavParams,
    public loadingCtrl: LoadingController,
    public alertCtrl: AlertController,
    private plt: Platform,
    private file: File,
    private fs: FirebaseService,
    private globals: GlobalVariable) {

    let videoData = navParams.get("videoData");
    this.filePath = videoData/*[0].fullPath*/;
    // this.fileName = videoData[0].name;
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad VideoUploading');
  }

  saveVideo(videoFile) {
    console.log(this.title);
    console.log(this.description);
    
  }

}
