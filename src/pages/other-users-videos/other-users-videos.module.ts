import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { OtherUsersVideos } from './other-users-videos';

@NgModule({
  declarations: [
    OtherUsersVideos,
  ],
  imports: [
    IonicPageModule.forChild(OtherUsersVideos),
  ],
  exports: [
    OtherUsersVideos
  ]
})
export class OtherUsersVideosModule {}
