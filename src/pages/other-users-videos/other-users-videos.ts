import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';


@IonicPage()
@Component({
  selector: 'page-other-users-videos',
  templateUrl: 'other-users-videos.html',
})
export class OtherUsersVideos {
  videos: any;

  constructor(public navCtrl: NavController, public navParams: NavParams) {
    // this.videos = navParams.get("videos");
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad OtherUsersVideos');
  }

}
