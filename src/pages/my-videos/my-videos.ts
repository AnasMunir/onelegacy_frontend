import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { GlobalVariable } from '../../app/globals';
// import { NativeStorage } from '@ionic-native/native-storage';
import { FirebaseService } from '../../providers/firebase-service';
import firebase from 'firebase';
/**
 * Generated class for the MyVideos page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */
@IonicPage()
@Component({
  selector: 'page-my-videos',
  templateUrl: 'my-videos.html',
})
export class MyVideos {

  noVideosUploadedYet: boolean;
  videos: any;

  constructor(public navCtrl: NavController,
    public navParams: NavParams,
    private globals: GlobalVariable,
    private fs: FirebaseService,
    /*private ns: NativeStorage*/) {
    this.videos = this.fs.getUserVideosToDisplay()
    // .subscribe((snapshot) => {
    //   console.log(snapshot);
    // })
    // const videos = this.fs.getUserData()
    //   .subscribe(
    //   (snapshot) => {
    //     console.log(snapshot);
    //     videos.unsubscribe();
    //   }
    //   )
    // if (this.globals.videos.length == 0) {
    //   this.noVideosUploadedYet = true;
    // } else {
    //   this.noVideosUploadedYet = false;
    // }

    console.log("this.noVideosUploadedYet", this.noVideosUploadedYet);

  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad MyVideos');

  }
  deleteVideo(/*index*/key, storageNumber) {
    console.log(key);
    console.log(storageNumber);
    this.fs.delteVideo(key)
      .then((data) => console.log(data))
      .then(() => this.fs.getUserVideos())
      .catch((error) => console.error("error deleting video", error));
    let storageRef = firebase.storage().ref();
    let videoRef = storageRef.child(this.globals.current_userUID).child('videos').child(storageNumber.toString())
    videoRef.delete()
      .then(() => console.log("deleted from storage"))
      .catch((error) => console.error("error deleting from storage", error))
  }
  // async getVideo() {
  //   try {
  //     this.noVideosUploadedYet = false;
  //     const data = await this.ns.getItem(this.globals.current_userUID);
  //     this.globals.videoURL = data.videoURL;
  //   } catch (error) {
  //     this.noVideosUploadedYet = true;
  //   }
  // }
}
