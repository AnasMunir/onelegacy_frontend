import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, AlertController, LoadingController } from 'ionic-angular';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { GlobalVariable } from "../../app/globals";
import { AuthService } from '../../providers/auth-service';
import { FirebaseService } from '../../providers/firebase-service';
import { BarcodeScanner, BarcodeScannerOptions } from '@ionic-native/barcode-scanner';

import { EmailValidator } from '../../validators/email';


@IonicPage()
@Component({
  selector: 'page-landing',
  templateUrl: 'landing.html',
})
export class Landing {

  public submitAttempt: boolean = false;
  public loginForm: FormGroup;
  loader: any;

  constructor(public navCtrl: NavController,
    public formBuilder: FormBuilder,
    public loadingCtrl: LoadingController,
    public alertCtrl: AlertController,
    private gloabls: GlobalVariable,
    private barcodeScanner: BarcodeScanner,
    private fs: FirebaseService,
    private auth: AuthService) {

    this.loginForm = formBuilder.group({
      email: ['', Validators.compose([Validators.required, EmailValidator.isValid])],
      password: ['', Validators.compose([Validators.minLength(6), Validators.required])],
    })
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad Login');
  }

  async login() {
    this.loader = this.loadingCtrl.create({
      content: "Loggin In...",
      dismissOnPageChange: true
    });
    this.submitAttempt = true;
    if (!this.loginForm.valid) {
      console.log(' Some values were not given or were incorrect, please fill them');
    } else {
      this.loader.present();
      console.log('success!');
      console.log(this.loginForm.value);
      try {
        const data = await this.auth.loginUser(
          this.loginForm.value.email,
          this.loginForm.value.password, );
        this.gloabls.current_userUID = data.uid;
        this.fs.getUserVideos();
        this.navCtrl.setRoot('Home');
      } catch (error) {
        this.loginAlert(error);
      }
    }
  }

  loginAlert(error) {
    this.loader.dismiss();
    let alert = this.alertCtrl.create({
      title: "Login error",
      subTitle: error,
      buttons: ["OK"]
    })
    alert.present();
  }
  goToSignup() {
    // this.navCtrl.setRoot('Register');
    this.navCtrl.push('SignUp')
  }

  scanBarcode() {
    let options: BarcodeScannerOptions = {
      showFlipCameraButton: true,
      showTorchButton: true,
      formats: 'QR_CODE'
    }

    this.barcodeScanner.scan(options).then((barcodeData) => {
      console.log("Success! Barcode data is here", barcodeData);
      this.fs.getOtherUsersVideos(barcodeData.text)
        .take(1).subscribe(
        (snapshot) => {
          if (snapshot.videos) this.navCtrl.push("OtherUsersVideos", { videos: snapshot.videos })
        })
    }, (err) => {
      console.error("error scanning", err);
    });
  }

}