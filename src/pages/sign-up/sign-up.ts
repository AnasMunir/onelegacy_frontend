import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, LoadingController } from 'ionic-angular';
import { AuthService } from '../../providers/auth-service';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { EmailValidator } from '../../validators/email';


@IonicPage()
@Component({
  selector: 'page-sign-up',
  templateUrl: 'sign-up.html',
})
export class SignUp {

  submitAttempt: boolean = false;
  signupForm: FormGroup;
  loader: any;

  constructor(public navCtrl: NavController,
    public navParams: NavParams,
    public loadingCtrl: LoadingController,
    private formBuilder: FormBuilder,
    private auth: AuthService) {

    this.signupForm = formBuilder.group({
      userName: ['', Validators.required],
      email: ['', Validators.compose([Validators.required, EmailValidator.isValid])],
      dob: ['', Validators.required],
      contact: ['', Validators.compose([Validators.minLength(6), Validators.required])],
      address: [''],
      secondaryContact: [''],
      password: ['', Validators.compose([Validators.minLength(6), Validators.required])],
    });

  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad SignUp');
  }

  async signup() {
    this.loader = this.loadingCtrl.create({
      content: "Signing Up...",
      dismissOnPageChange: true
    });
    this.submitAttempt = true;
    // let age = this.currentYear - this.signupForm.value.dob.substring(0, 4);
    // console.log(this.signupForm.value.dob.substring(0, 4));
    if (!this.signupForm.valid) {
      console.log(' Some values were not given or were incorrect, please fill them');
    } else {
      this.loader.present();
      console.log('success!');
      console.log(this.signupForm.value);
      const data = await this.auth.signupUser(
        this.signupForm.value.email,
        this.signupForm.value.password,
        this.signupForm.value);
      console.log(data);
      this.navCtrl.setRoot('Home');
    }
  }

}
