import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { VideoPreview } from './video-preview';

@NgModule({
  declarations: [
    VideoPreview,
  ],
  imports: [
    IonicPageModule.forChild(VideoPreview),
  ],
  exports: [
    VideoPreview
  ]
})
export class VideoPreviewModule {}
