import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

@IonicPage()
@Component({
  selector: 'page-video-preview',
  templateUrl: 'video-preview.html',
})
export class VideoPreview {

  fileName: string;
  filePath: string;
  // localUrl: string;

  constructor(public navCtrl: NavController, public navParams: NavParams) {

    let videoData = navParams.get('videoData');
    this.filePath = videoData[0].fullPath;
    this.fileName = videoData[0].name;
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad VideoPreview');
  }

  recordNew() {
    this.navCtrl.pop();
  }

  proceed() {
    
  }

}
