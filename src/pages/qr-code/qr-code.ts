import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { QRCodeComponent } from 'angular2-qrcode';
import { GlobalVariable } from '../../app/globals';
import { BarcodeScanner } from '@ionic-native/barcode-scanner';

@IonicPage()
@Component({
  selector: 'page-qr-code',
  templateUrl: 'qr-code.html',
})
export class QrCode {

  qrcodeValue: string = "";

  constructor(public navCtrl: NavController,
    public navParams: NavParams,
    private globals: GlobalVariable,
    private barcodeScanner: BarcodeScanner) {

    this.qrcodeValue = this.globals.current_userUID;
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad QrCode');
  }

  shareQrCode() {

    this.barcodeScanner.encode(this.barcodeScanner.Encode.TEXT_TYPE, this.qrcodeValue)
      .then(value => console.log(value));
  }

}
