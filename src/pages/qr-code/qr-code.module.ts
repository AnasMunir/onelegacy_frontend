import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { QrCode } from './qr-code';
import { QRCodeModule } from 'angular2-qrcode';

@NgModule({
  declarations: [
    QrCode,
  ],
  imports: [
    IonicPageModule.forChild(QrCode),
    QRCodeModule
  ],
  exports: [
    QrCode
  ]
})
export class QrCodeModule {}
