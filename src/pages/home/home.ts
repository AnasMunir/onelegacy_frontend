import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, Platform, ToastController } from 'ionic-angular';
import { BarcodeScanner, BarcodeScannerOptions } from '@ionic-native/barcode-scanner';
import { MediaCapture, CaptureVideoOptions, MediaFile, CaptureError } from '@ionic-native/media-capture';
import { File } from '@ionic-native/file';
import { Camera, CameraOptions } from '@ionic-native/camera';
import { FirebaseService } from '../../providers/firebase-service';
import { GlobalVariable } from '../../app/globals';
// import { VideoEditor } from '@ionic-native/video-editor';

/**
 * Generated class for the Home page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */
@IonicPage()
@Component({
  selector: 'page-home',
  templateUrl: 'home.html',
})
export class Home {

  videoOptions: CaptureVideoOptions;
  fileName: string;
  filePath: string;
  localUrl: string;
  userData: any;

  constructor(public navCtrl: NavController,
    public navParams: NavParams,
    private plt: Platform,
    private barcodeScanner: BarcodeScanner,
    private mediaCapture: MediaCapture,
    private file: File,
    private camera: Camera,
    private fs: FirebaseService,
    private globals: GlobalVariable,
    private toastCtrl: ToastController) {

  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad Home');
  }
  ionViewWillEnter() {
    const userSubscription = this.fs.getUserData()
      .subscribe(
      (snapshot) => {
        console.log(snapshot);
        this.userData = snapshot;
        userSubscription.unsubscribe();
      }
      );
  }

  async takeVideo(method) {
    this.navCtrl.push("VideoUploading", { videoData: this.filePath });
  }


  scanBarcode() {
    this.navCtrl.push("OtherUsersVideos"/*, { videos: snapshot.videos }*/)
    
  }

  myVideos() {
    if (this.userData.videos) {
      this.navCtrl.push("MyVideos");
    } else {
      let toast = this.toastCtrl.create({
        message: "You have'nt upload any videos",
        duration: 3000,
        position: 'bottom'
      });

      toast.onDidDismiss(() => {
        console.log('Dismissed toast');
      });

      toast.present();
    }
  }

  async selectVideo() {
    let options: CameraOptions = {
      sourceType: 0,
      mediaType: 1

    };

    let data = await this.camera.getPicture(options);
    let videoFile = "file://" + data;
    console.log("video data:", videoFile);
    // this.uploadToFirebase(videoFile);
    this.navCtrl.push("VideoUploading", { videoData: videoFile });
  }

}
