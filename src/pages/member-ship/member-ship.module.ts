import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { MemberShip } from './member-ship';

@NgModule({
  declarations: [
    MemberShip,
  ],
  imports: [
    IonicPageModule.forChild(MemberShip),
  ],
  exports: [
    MemberShip
  ]
})
export class MemberShipModule {}
