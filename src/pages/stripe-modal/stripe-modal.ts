import { Component, OnInit } from '@angular/core';
import { IonicPage, NavController, NavParams, ViewController, AlertController, LoadingController, App } from 'ionic-angular';
import { GlobalVariable } from "../../app/globals";
import { Stripe } from '@ionic-native/stripe';
import { NativeStorage } from '@ionic-native/native-storage';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

// import { FirebaseService } from "../../providers/firebase-service";
import { PaymentService } from '../../providers/payment-service';
import { FirebaseService } from '../../providers/firebase-service';

@IonicPage()
@Component({
  selector: 'page-stripe-modal',
  templateUrl: 'stripe-modal.html',
})
export class StripeModal {

  public submitAttempt: boolean = false;
  public validCard: boolean = false;
  public validCvv: boolean = false;
  public validExpDate: boolean = false;
  loader: any;
  public stripeForm: FormGroup;
  maxYear; minYear;
  status: string;
  upgrade: boolean;
  currentStatus: string;
  displayAmount: string;
  chargeAmount: string;
  chargType: string;

  constructor(
    // private app: App,
    public navCtrl: NavController,
    public navParams: NavParams,
    public viewCtrl: ViewController,
    public alertCtrl: AlertController,
    public loadingCtrl: LoadingController,
    public formBuilder: FormBuilder,
    private fs: FirebaseService,
    private ps: PaymentService,
    private stripe: Stripe,
    private nativeStorage: NativeStorage,
    private globals: GlobalVariable) {

    this.status = navParams.get("status");
    this.upgrade = navParams.get("upgrade");
    this.currentStatus = navParams.get("currentStatus");

    this.stripeForm = formBuilder.group({
      cardNumber: ['', Validators.compose([Validators.pattern('[0-9]{16}'), Validators.required])],
      cvv: ['', Validators.compose([Validators.pattern('[0-9]{3}'), Validators.required])],
      expMonth: ['', Validators.required],
      expYear: ['', Validators.required]
    })

    this.loader = loadingCtrl.create({
      content: "Please wait...",
      dismissOnPageChange: true
    })

    this.displayAmount = "$1050";
    this.chargeAmount = "105000";
    this.chargType = "Premium";

  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad StripeModal');
  }

  ionViewWillEnter() {
    let currentYear = new Date().getFullYear();
    this.maxYear = currentYear + 5;
    this.minYear = currentYear;
    console.log("maxYear: ", this.maxYear);
    console.log("minYear: ", this.minYear);
    this.stripe.setPublishableKey(this.globals.publishableKey);
    
  }

  dismiss() {
    this.viewCtrl.dismiss();
  }

  async pay() {
    this.submitAttempt = true;
    if (!this.stripeForm.valid) {
      console.log(' Some values were not given or were incorrect, please fill them');
    } else {
      let cardNumber = this.stripeForm.value.cardNumber;
      let expYear = this.stripeForm.value.expYear;
      let expMonth = this.stripeForm.value.expMonth;
      let cvv = this.stripeForm.value.cvv;
      let card = {
        number: cardNumber,
        expMonth: expMonth,
        expYear: expYear,
        cvc: cvv
      };

      console.log(this.stripeForm.value);
      try {
        this.loader.present();
        
        // const token = await this.createCardToken(cardNumber, expYear, expMonth, cvv);
        const token = await this.stripe.createCardToken(card)
        // this.charge(token);
        console.log('token: ', token);
        const stripeResponse = await this.ps.stripePayment(this.chargeAmount, token, "process fees");
        console.log(stripeResponse);
        await this.loader.dismiss();
        
      } catch (error) {
        this.loader.dismiss();
        
        console.error("error validation card: ", error);
      }
    }
  }

  

  

}
