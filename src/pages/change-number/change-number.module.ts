import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ChangeNumber } from './change-number';

@NgModule({
  declarations: [
    ChangeNumber,
  ],
  imports: [
    IonicPageModule.forChild(ChangeNumber),
  ],
  exports: [
    ChangeNumber
  ]
})
export class ChangeNumberModule {}
