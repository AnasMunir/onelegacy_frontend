import { NgModule, ErrorHandler } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { IonicApp, IonicModule, IonicErrorHandler } from 'ionic-angular';
import { HttpModule } from '@angular/http';
import { MyApp } from './app.component';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import { BarcodeScanner } from '@ionic-native/barcode-scanner';
import { MediaCapture } from '@ionic-native/media-capture';
import { Camera } from '@ionic-native/camera';
import { File } from '@ionic-native/file';
import { NativeStorage } from '@ionic-native/native-storage';
import { Stripe } from '@ionic-native/stripe';
// import { VideoEditor } from '@ionic-native/video-editor';

import { FirebaseService } from '../providers/firebase-service';
import { PaymentService } from '../providers/payment-service';
import { AuthService } from '../providers/auth-service';
import { GlobalVariable } from './globals';

// importing angularire 2 
import { AngularFireModule } from 'angularfire2';
import { AngularFireDatabaseModule } from 'angularfire2/database';
import { AngularFireAuthModule } from "angularfire2/auth";
import { QRCodeModule } from 'angular2-qrcode';

export const firebaseConfig = {
  apiKey: "AIzaSyDyK596cuHRTzGK_CHQIYHk4hIQJFb7W1k",
  authDomain: "onelegacy-f0695.firebaseapp.com",
  databaseURL: "https://onelegacy-f0695.firebaseio.com",
  projectId: "onelegacy-f0695",
  storageBucket: "onelegacy-f0695.appspot.com",
  messagingSenderId: "107640986523"
};


@NgModule({
  declarations: [
    MyApp
  ],
  imports: [
    BrowserModule,
    HttpModule,
    IonicModule.forRoot(MyApp),
    AngularFireModule.initializeApp(firebaseConfig),
    AngularFireDatabaseModule,
    AngularFireAuthModule,
    QRCodeModule
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp
  ],
  providers: [
    StatusBar,
    SplashScreen,
    BarcodeScanner,
    MediaCapture,
    // VideoEditor,
    File,
    Camera,
    NativeStorage,
    Stripe,
    AuthService,
    GlobalVariable,
    FirebaseService,
    PaymentService,
    { provide: ErrorHandler, useClass: IonicErrorHandler }
  ]
})
export class AppModule { }
