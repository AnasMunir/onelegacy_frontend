import { Injectable } from '@angular/core'
import { FormGroup } from '@angular/forms';

@Injectable()
export class GlobalVariable {
  public userName: string = "";
  public current_userUID = "";
  public videoURL = "";
  public email: string = "";
  public dob: any;
  public contact: string = "";
  public address: string = "";
  public secondaryContact: string = "";
  public videos: Array<{ video: string, title: string, description: string }> = [];
  public publishableKey = "pk_test_iog9Th3sTbuvIIqoNoAKtX1O";
  public membership: String;
}