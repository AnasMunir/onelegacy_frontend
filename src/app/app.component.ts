import { Component, ViewChild } from '@angular/core';
import { Platform, Nav, MenuController } from 'ionic-angular';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
// import { /*AuthProviders,*/ /*AuthMethods */} from 'angularfire2';
import { AngularFireAuth } from 'angularfire2/auth';
import { AuthService } from '../providers/auth-service';
import { GlobalVariable } from './globals';
import { FirebaseService } from '../providers/firebase-service';

@Component({
  templateUrl: 'app.html'
})
export class MyApp {
  @ViewChild(Nav) nav: Nav;

  rootPage: any = 'Profile';
  paymentStatus: string = "Testing";
  subscription: any;

  constructor(platform: Platform,
    statusBar: StatusBar,
    splashScreen: SplashScreen,
    private menu: MenuController,
    private as: AuthService,
    private fs: FirebaseService,
    private globals: GlobalVariable,
    private afAuth: AngularFireAuth) {

    // const authObserver = afAuth.authState.subscribe(user => {
    //   if (user) {
    //     // this.rootPage = TabsPage;
    //     console.log(user);
    //     console.log("logging from app component: ", user.uid);
    //     globals.current_userUID = user.uid;
    //     this.getPaymentStatus();
    //     this.rootPage = 'Home';
    //     authObserver.unsubscribe();
    //     // this.checkAccountStatus(user.uid);
    //     // this.backgroundCheckStatus(user.uid);
    //   } else {
    //     this.rootPage = 'Landing';
    //     authObserver.unsubscribe();
    //   }
    // });
    platform.ready().then(() => {
      // Okay, so the platform is ready and our plugins are available.
      // Here you can do any higher level native things you might need.
      statusBar.styleDefault();
      splashScreen.hide();
    });
  }

  qrCode() {
    this.menu.close();
    this.nav.setRoot("QrCode");
  }

  home() {
    this.menu.close();
    this.nav.setRoot("Home");
  }

  membership() {
    this.menu.close();
    this.nav.setRoot("MemberShip");
  }

  async logout() {
    await this.as.logoutUser()
    this.menu.close();
    this.nav.setRoot("Landing")
  }

  getPaymentStatus() {
    /*this.subscription = */this.fs.getPaymentStatus().subscribe(
      (snapshot) => {
        this.paymentStatus = snapshot.$value;
        this.globals.membership = snapshot.$value;
      }
    );
  }
}
